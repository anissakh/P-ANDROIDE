[37;1m [38;5;81m UE vlsi1 (21) :
	Nombre de groupes : 2
	Capacite totale d'accueil: 64
	Equilibre? 20.0%: Oui
	Nombre Etudiants interesses: 42
	Nombre Etudiants effectivement inscrits : 42
	Les (0) Non-inscrits : 
	Les Inscrits:
		Groupe 1 [21/32] : 3(sesi) 11(sesi) 16(sesi) 17(sesi) 20(sesi) 21(sesi) 25(sesi) 26(sesi) 29(sesi) 30(sesi) 31(sesi) 33(sesi) 34(sesi) 37(sesi) 38(sesi) 39(sesi) 43(sesi) 44(sesi) 45(sesi) 49(sesi) 50(sesi) 
		Groupe 2 [21/32] : 1(sesi) 2(sesi) 4(sesi) 5(sesi) 6(sesi) 7(sesi) 8(sesi) 9(sesi) 10(sesi) 12(sesi) 13(sesi) 15(sesi) 18(sesi) 19(sesi) 28(sesi) 35(sesi) 40(sesi) 41(sesi) 42(sesi) 46(sesi) 48(sesi) 


[37;1m[38;5;155m UE signal (20) :
	Nombre de groupes : 2
	Capacite totale d'accueil: 64
	Equilibre? 20.0%: Oui
	Nombre Etudiants interesses: 50
	Nombre Etudiants effectivement inscrits : 50
	Les (0) Non-inscrits : 
	Les Inscrits:
		Groupe 1 [25/32] : 11(res) 13(res) 23(res) 29(res) 34(res) 37(res) 45(res) 49(res) 51(res) 1(sesi) 2(sesi) 4(sesi) 5(sesi) 6(sesi) 8(sesi) 9(sesi) 10(sesi) 12(sesi) 28(sesi) 35(sesi) 36(sesi) 40(sesi) 41(sesi) 42(sesi) 48(sesi) 
		Groupe 2 [25/32] : 1(res) 4(res) 7(res) 17(res) 18(res) 19(res) 22(res) 24(res) 26(res) 28(res) 33(res) 36(res) 42(res) 48(res) 11(sesi) 13(sesi) 18(sesi) 21(sesi) 22(sesi) 23(sesi) 25(sesi) 27(sesi) 29(sesi) 31(sesi) 38(sesi) 


[37;1m[38;5;190m UE rtel (19) :
	Nombre de groupes : 2
	Capacite totale d'accueil: 64
	Equilibre? 20.0%: Oui
	Nombre Etudiants interesses: 49
	Nombre Etudiants effectivement inscrits : 49
	Les (0) Non-inscrits : 
	Les Inscrits:
		Groupe 1 [25/32] : 3(res) 4(res) 6(res) 7(res) 10(res) 16(res) 20(res) 28(res) 30(res) 31(res) 33(res) 36(res) 39(res) 40(res) 42(res) 43(res) 44(res) 46(res) 47(res) 48(res) 50(res) 3(sar) 12(sar) 21(sar) 26(sar) 
		Groupe 2 [24/32] : 1(res) 2(res) 5(res) 11(res) 12(res) 14(res) 15(res) 17(res) 21(res) 23(res) 24(res) 25(res) 26(res) 27(res) 34(res) 35(res) 37(res) 38(res) 41(res) 45(res) 49(res) 51(res) 25(sar) 14(sesi) 


[37;1m[38;5;12m UE pr (18) :
	Nombre de groupes : 4
	Capacite totale d'accueil: 128
	Equilibre? 20.0%: Oui
	Nombre Etudiants interesses: 110
	Nombre Etudiants effectivement inscrits : 110
	Les (0) Non-inscrits : 
	Les Inscrits:
		Groupe 1 [27/32] : 32(res) 2(sar) 8(sar) 9(sar) 10(sar) 11(sar) 14(sar) 33(sar) 4(stl) 29(stl) 31(stl) 40(stl) 47(stl) 1(sfpn) 4(sfpn) 14(sfpn) 22(sfpn) 11(sesi) 16(sesi) 17(sesi) 20(sesi) 21(sesi) 24(sesi) 26(sesi) 34(sesi) 49(sesi) 50(sesi) 
		Groupe 2 [28/32] : 4(res) 5(res) 7(res) 21(res) 28(res) 33(res) 36(res) 41(res) 42(res) 48(res) 49(res) 1(sar) 12(sar) 15(sar) 20(sar) 31(sar) 32(sar) 13(stl) 41(stl) 46(stl) 59(stl) 6(sfpn) 15(sfpn) 16(sfpn) 18(sfpn) 19(sfpn) 26(sfpn) 27(sfpn) 
		Groupe 3 [27/32] : 31(res) 3(sar) 7(sar) 13(sar) 21(sar) 22(sar) 24(sar) 26(sar) 28(sar) 30(sar) 6(stl) 17(stl) 19(stl) 20(stl) 21(stl) 26(stl) 28(stl) 43(stl) 57(stl) 3(sfpn) 5(sfpn) 13(sfpn) 17(sfpn) 20(sfpn) 21(sfpn) 23(sfpn) 25(sfpn) 
		Groupe 4 [28/32] : 3(res) 8(res) 12(res) 20(res) 30(res) 38(res) 43(res) 44(res) 46(res) 4(sar) 5(sar) 17(sar) 18(sar) 19(sar) 23(sar) 25(sar) 27(sar) 2(stl) 3(stl) 5(stl) 7(stl) 9(stl) 18(stl) 22(stl) 56(stl) 7(sfpn) 3(sesi) 30(sesi) 


[37;1m[38;5;209m UE noyau (17) :
	Nombre de groupes : 4
	Capacite totale d'accueil: 128
	Equilibre? 20.0%: Oui
	Nombre Etudiants interesses: 99
	Nombre Etudiants effectivement inscrits : 99
	Les (0) Non-inscrits : 
	Les Inscrits:
		Groupe 1 [25/32] : 5(res) 7(res) 12(res) 16(res) 20(res) 23(res) 26(res) 46(res) 48(res) 50(res) 3(sar) 6(sar) 12(sar) 13(sar) 23(sar) 25(sar) 31(sar) 32(sar) 41(stl) 7(sfpn) 12(sfpn) 18(sfpn) 22(sfpn) 24(sfpn) 24(sesi) 
		Groupe 2 [25/32] : 1(res) 17(res) 21(res) 24(res) 30(res) 39(res) 41(res) 43(res) 51(res) 1(sar) 2(sar) 4(sar) 9(sar) 10(sar) 16(sar) 17(sar) 18(sar) 22(sar) 24(sar) 28(sar) 12(stl) 27(stl) 1(sfpn) 20(sfpn) 7(sesi) 
		Groupe 3 [24/32] : 35(res) 37(res) 38(res) 45(res) 7(sar) 8(sar) 27(sar) 19(stl) 24(stl) 2(sfpn) 21(sfpn) 3(sesi) 13(sesi) 16(sesi) 17(sesi) 20(sesi) 21(sesi) 26(sesi) 33(sesi) 34(sesi) 39(sesi) 45(sesi) 46(sesi) 50(sesi) 
		Groupe 4 [25/32] : 28(res) 31(res) 44(res) 5(sar) 11(sar) 15(sar) 19(sar) 20(sar) 21(sar) 26(sar) 33(sar) 13(stl) 32(stl) 46(stl) 47(stl) 48(stl) 54(stl) 3(sfpn) 4(sfpn) 11(sfpn) 14(sfpn) 15(sfpn) 16(sfpn) 19(sfpn) 26(sfpn) 


[37;1m[38;5;100m UE mogpl (16) :
	Nombre de groupes : 4
	Capacite totale d'accueil: 128
	Equilibre? 20.0%: Oui
	Nombre Etudiants interesses: 112
	Nombre Etudiants effectivement inscrits : 112
	Les (0) Non-inscrits : 
	Les Inscrits:
		Groupe 1 [28/32] : 6(res) 25(res) 27(res) 40(res) 1(dac) 1(ima) 2(ima) 3(ima) 5(ima) 6(ima) 7(ima) 8(ima) 12(ima) 3(and) 9(and) 13(and) 15(and) 16(and) 23(and) 34(and) 41(and) 43(and) 48(and) 8(stl) 10(stl) 16(stl) 36(stl) 52(stl) 
		Groupe 2 [28/32] : 1(res) 3(res) 4(res) 11(res) 32(res) 47(res) 5(dac) 8(dac) 13(dac) 24(dac) 28(dac) 30(dac) 34(dac) 35(dac) 37(dac) 18(bim) 2(and) 5(and) 19(and) 21(and) 28(and) 32(and) 33(and) 36(and) 37(and) 39(and) 46(and) 47(and) 
		Groupe 3 [28/32] : 12(res) 34(res) 38(res) 42(res) 50(res) 2(dac) 6(dac) 10(dac) 12(dac) 22(dac) 32(dac) 6(bim) 17(bim) 1(and) 6(and) 7(and) 10(and) 11(and) 12(and) 20(and) 25(and) 26(and) 30(and) 31(and) 35(and) 40(and) 42(and) 45(and) 
		Groupe 4 [28/32] : 35(res) 3(dac) 11(dac) 14(dac) 16(dac) 17(dac) 18(dac) 19(dac) 27(dac) 31(dac) 9(bim) 10(bim) 12(bim) 13(bim) 14(bim) 21(bim) 4(and) 8(and) 14(and) 17(and) 18(and) 22(and) 24(and) 38(and) 11(stl) 23(stl) 40(stl) 7(sesi) 


[37;1m[38;5;124m UE model (15) :
	Nombre de groupes : 1
	Capacite totale d'accueil: 50
	Equilibre? 20.0%: Oui
	Nombre Etudiants interesses: 36
	Nombre Etudiants effectivement inscrits : 36
	Les (0) Non-inscrits : 
	Les Inscrits:
		Groupe 1 [36/50] : 7(dac) 21(dac) 23(dac) 29(dac) 34(dac) 36(dac) 1(ima) 2(ima) 5(ima) 7(ima) 12(ima) 3(bim) 34(stl) 47(stl) 1(sfpn) 2(sfpn) 3(sfpn) 4(sfpn) 5(sfpn) 6(sfpn) 8(sfpn) 10(sfpn) 11(sfpn) 12(sfpn) 13(sfpn) 14(sfpn) 15(sfpn) 16(sfpn) 17(sfpn) 18(sfpn) 19(sfpn) 20(sfpn) 21(sfpn) 22(sfpn) 23(sfpn) 27(sfpn) 


[37;1m[38;5;166m UE mobj (14) :
	Nombre de groupes : 2
	Capacite totale d'accueil: 32
	Equilibre? 20.0%: Oui
	Nombre Etudiants interesses: 28
	Nombre Etudiants effectivement inscrits : 28
	Les (0) Non-inscrits : 
	Les Inscrits:
		Groupe 1 [14/16] : 20(sar) 6(sesi) 9(sesi) 10(sesi) 17(sesi) 28(sesi) 29(sesi) 34(sesi) 39(sesi) 40(sesi) 41(sesi) 42(sesi) 45(sesi) 48(sesi) 
		Groupe 2 [14/16] : 1(sesi) 2(sesi) 4(sesi) 5(sesi) 8(sesi) 12(sesi) 15(sesi) 16(sesi) 20(sesi) 26(sesi) 30(sesi) 33(sesi) 35(sesi) 49(sesi) 


[37;1m[38;5;96m UE mlbda (13) :
	Nombre de groupes : 4
	Capacite totale d'accueil: 128
	Equilibre? 20.0%: Oui
	Nombre Etudiants interesses: 126
	Nombre Etudiants effectivement inscrits : 126
	Les (0) Non-inscrits : 
	Les Inscrits:
		Groupe 1 [32/32] : 2(dac) 3(dac) 7(dac) 11(dac) 14(dac) 16(dac) 18(dac) 21(dac) 23(dac) 29(dac) 36(dac) 18(sar) 28(sar) 5(bim) 6(and) 14(and) 17(and) 21(and) 27(and) 29(and) 46(and) 3(stl) 6(stl) 14(stl) 17(stl) 22(stl) 27(stl) 43(stl) 45(stl) 5(sfpn) 13(sfpn) 37(sesi) 
		Groupe 2 [31/32] : 19(dac) 34(dac) 6(sar) 16(sar) 29(sar) 2(and) 7(and) 11(and) 20(and) 22(and) 25(and) 26(and) 28(and) 31(and) 32(and) 34(and) 35(and) 41(and) 47(and) 11(stl) 21(stl) 26(stl) 28(stl) 30(stl) 37(stl) 42(stl) 55(stl) 58(stl) 30(sesi) 43(sesi) 47(sesi) 
		Groupe 3 [31/32] : 30(res) 31(res) 32(res) 50(res) 5(dac) 6(dac) 8(dac) 10(dac) 17(dac) 25(dac) 27(dac) 28(dac) 31(dac) 5(sar) 14(sar) 17(sar) 19(sar) 23(sar) 24(sar) 30(sar) 4(bim) 11(bim) 5(and) 18(and) 19(and) 1(stl) 5(stl) 9(stl) 48(stl) 56(stl) 57(stl) 
		Groupe 4 [32/32] : 6(res) 10(res) 14(res) 15(res) 16(res) 40(res) 47(res) 1(dac) 4(dac) 9(dac) 12(dac) 13(dac) 15(dac) 20(dac) 26(dac) 30(dac) 33(dac) 35(dac) 37(dac) 1(and) 10(and) 13(and) 37(and) 45(and) 12(stl) 15(stl) 35(stl) 39(stl) 44(stl) 49(stl) 50(stl) 53(stl) 


[37;1m[38;5;63m UE maths4m062 (12) :
	Nombre de groupes : 1
	Capacite totale d'accueil: 32
	Equilibre? 20.0%: Oui
	Nombre Etudiants interesses: 19
	Nombre Etudiants effectivement inscrits : 19
	Les (0) Non-inscrits : 
	Les Inscrits:
		Groupe 1 [19/32] : 1(bim) 2(bim) 7(bim) 8(bim) 9(bim) 10(bim) 12(bim) 13(bim) 14(bim) 15(bim) 16(bim) 18(bim) 19(bim) 20(bim) 21(bim) 22(bim) 23(bim) 24(bim) 25(bim) 


[37;1m[38;5;70m UE mapsi (11) :
	Nombre de groupes : 5
	Capacite totale d'accueil: 144
	Equilibre? 20.0%: Oui
	Nombre Etudiants interesses: 126
	Nombre Etudiants effectivement inscrits : 126
	Les (0) Non-inscrits : 
	Les Inscrits:
		Groupe 1 [28/32] : 1(dac) 8(dac) 10(dac) 12(dac) 13(dac) 28(dac) 30(dac) 31(dac) 32(dac) 3(ima) 9(ima) 11(ima) 12(ima) 9(bim) 17(bim) 5(and) 10(and) 15(and) 34(and) 36(and) 37(and) 44(and) 55(stl) 2(sfpn) 19(sfpn) 23(sfpn) 43(sesi) 46(sesi) 
		Groupe 2 [28/32] : 2(dac) 9(dac) 14(dac) 15(dac) 24(dac) 29(dac) 36(dac) 37(dac) 1(ima) 4(ima) 6(ima) 7(ima) 8(ima) 2(bim) 11(bim) 16(bim) 19(bim) 22(bim) 25(bim) 8(and) 20(and) 23(and) 24(and) 25(and) 26(and) 32(and) 35(and) 45(and) 
		Groupe 3 [14/16] : 23(dac) 35(dac) 10(bim) 1(and) 6(and) 7(and) 11(and) 21(and) 33(and) 39(and) 42(and) 46(and) 17(sfpn) 25(sfpn) 
		Groupe 4 [28/32] : 4(dac) 5(dac) 6(dac) 20(dac) 26(dac) 27(dac) 2(ima) 5(ima) 8(bim) 12(bim) 13(bim) 14(bim) 18(bim) 24(bim) 2(and) 3(and) 4(and) 12(and) 14(and) 16(and) 18(and) 19(and) 28(and) 30(and) 31(and) 40(and) 43(and) 47(and) 
		Groupe 5 [28/32] : 3(dac) 7(dac) 11(dac) 16(dac) 17(dac) 18(dac) 19(dac) 21(dac) 10(ima) 1(bim) 4(bim) 5(bim) 7(bim) 15(bim) 20(bim) 21(bim) 23(bim) 9(and) 13(and) 22(and) 38(and) 41(and) 5(sfpn) 8(sfpn) 9(sfpn) 10(sfpn) 13(sfpn) 21(sfpn) 


[37;1m[38;5;37m UE lrc (10) :
	Nombre de groupes : 5
	Capacite totale d'accueil: 160
	Equilibre? 20.0%: Oui
	Nombre Etudiants interesses: 137
	Nombre Etudiants effectivement inscrits : 137
	Les (0) Non-inscrits : 
	Les Inscrits:
		Groupe 1 [28/32] : 2(dac) 4(dac) 5(dac) 9(dac) 11(dac) 14(dac) 15(dac) 16(dac) 18(dac) 19(dac) 20(dac) 21(dac) 23(dac) 26(dac) 27(dac) 28(dac) 30(dac) 35(dac) 10(ima) 2(and) 7(and) 10(and) 11(and) 17(and) 28(and) 35(and) 47(and) 1(stl) 
		Groupe 2 [27/32] : 2(res) 6(dac) 8(dac) 10(dac) 13(dac) 17(dac) 29(dac) 31(dac) 4(ima) 1(and) 4(and) 5(and) 6(and) 18(and) 19(and) 20(and) 21(and) 24(and) 25(and) 26(and) 32(and) 36(and) 37(and) 38(and) 45(and) 46(and) 55(stl) 
		Groupe 3 [28/32] : 9(res) 10(res) 14(res) 22(res) 34(res) 33(dac) 34(dac) 5(sar) 19(sar) 27(and) 2(stl) 4(stl) 7(stl) 8(stl) 15(stl) 16(stl) 18(stl) 20(stl) 23(stl) 26(stl) 32(stl) 33(stl) 37(stl) 38(stl) 39(stl) 40(stl) 44(stl) 45(stl) 
		Groupe 4 [27/32] : 1(dac) 12(dac) 37(dac) 4(sar) 18(sar) 28(sar) 8(ima) 7(bim) 20(bim) 22(bim) 23(bim) 8(and) 9(and) 13(and) 14(and) 15(and) 16(and) 22(and) 23(and) 31(and) 35(stl) 36(stl) 49(stl) 53(stl) 58(stl) 7(sesi) 43(sesi) 
		Groupe 5 [27/32] : 39(res) 3(dac) 7(dac) 30(sar) 6(ima) 12(ima) 2(bim) 3(bim) 5(bim) 10(bim) 15(bim) 18(bim) 19(bim) 21(bim) 24(bim) 3(and) 12(and) 33(and) 39(and) 40(and) 42(and) 43(and) 48(and) 14(stl) 43(stl) 51(stl) 52(stl) 


[37;1m[38;5;53m UE il (9) :
	Nombre de groupes : 5
	Capacite totale d'accueil: 160
	Equilibre? 20.0%: Oui
	Nombre Etudiants interesses: 142
	Nombre Etudiants effectivement inscrits : 124
	Les (18) Non-inscrits : 11(res) 27(res) 39(res) 3(dac) 7(dac) 34(dac) 2(sar) 25(sar) 22(and) 38(and) 39(and) 42(and) 48(and) 4(stl) 11(stl) 14(stl) 37(stl) 58(stl) 
	Les Inscrits:
		Groupe 1 [25/32] : 9(res) 18(res) 5(dac) 6(dac) 20(dac) 27(dac) 35(dac) 37(dac) 4(sar) 5(sar) 19(sar) 13(and) 16(and) 23(and) 7(stl) 9(stl) 13(stl) 16(stl) 18(stl) 36(stl) 39(stl) 41(stl) 44(stl) 53(stl) 59(stl) 
		Groupe 2 [25/32] : 9(dac) 15(dac) 17(dac) 26(dac) 17(sar) 27(sar) 32(sar) 4(ima) 8(ima) 18(and) 19(and) 2(stl) 5(stl) 8(stl) 12(stl) 15(stl) 19(stl) 20(stl) 32(stl) 33(stl) 35(stl) 48(stl) 49(stl) 56(stl) 57(stl) 
		Groupe 3 [25/32] : 19(res) 22(res) 1(dac) 8(dac) 10(dac) 11(dac) 12(dac) 13(dac) 16(dac) 18(dac) 21(dac) 25(dac) 28(dac) 31(dac) 33(dac) 10(ima) 1(bim) 4(bim) 7(bim) 20(bim) 23(bim) 5(and) 9(and) 15(and) 27(and) 
		Groupe 4 [25/32] : 3(res) 8(res) 13(res) 47(res) 13(sar) 21(sar) 22(sar) 24(sar) 3(ima) 5(ima) 11(ima) 6(bim) 29(and) 44(and) 3(stl) 6(stl) 10(stl) 17(stl) 22(stl) 27(stl) 50(stl) 54(stl) 22(sesi) 37(sesi) 44(sesi) 
		Groupe 5 [24/32] : 6(res) 10(res) 16(res) 40(res) 43(res) 14(dac) 29(dac) 36(dac) 3(sar) 12(sar) 18(sar) 28(sar) 31(sar) 9(ima) 9(bim) 11(bim) 14(bim) 16(bim) 22(bim) 8(and) 14(and) 1(stl) 38(stl) 45(stl) 


[37;1m[38;5;9m UE esa (8) :
	Nombre de groupes : 2
	Capacite totale d'accueil: 32
	Equilibre? 20.0%: Oui
	Nombre Etudiants interesses: 22
	Nombre Etudiants effectivement inscrits : 22
	Les (0) Non-inscrits : 
	Les Inscrits:
		Groupe 1 [11/16] : 6(sesi) 9(sesi) 10(sesi) 19(sesi) 28(sesi) 32(sesi) 40(sesi) 41(sesi) 42(sesi) 48(sesi) 50(sesi) 
		Groupe 2 [11/16] : 1(sesi) 2(sesi) 4(sesi) 5(sesi) 8(sesi) 11(sesi) 12(sesi) 13(sesi) 15(sesi) 27(sesi) 35(sesi) 


[37;1m[38;5;7m UE dlp (7) :
	Nombre de groupes : 3
	Capacite totale d'accueil: 96
	Equilibre? 20.0%: Oui
	Nombre Etudiants interesses: 81
	Nombre Etudiants effectivement inscrits : 81
	Les (0) Non-inscrits : 
	Les Inscrits:
		Groupe 1 [27/32] : 2(res) 18(res) 2(sar) 17(sar) 24(sar) 32(sar) 6(stl) 10(stl) 18(stl) 20(stl) 23(stl) 26(stl) 28(stl) 31(stl) 35(stl) 37(stl) 39(stl) 44(stl) 49(stl) 50(stl) 51(stl) 52(stl) 53(stl) 55(stl) 56(stl) 57(stl) 58(stl) 
		Groupe 2 [27/32] : 14(res) 15(res) 25(res) 16(sar) 23(sar) 27(sar) 2(stl) 3(stl) 5(stl) 7(stl) 8(stl) 9(stl) 11(stl) 12(stl) 14(stl) 15(stl) 16(stl) 17(stl) 19(stl) 21(stl) 22(stl) 27(stl) 36(stl) 40(stl) 42(stl) 6(sfpn) 23(sfpn) 
		Groupe 3 [27/32] : 1(sar) 8(sar) 9(sar) 10(sar) 15(sar) 31(sar) 11(ima) 1(stl) 4(stl) 13(stl) 24(stl) 25(stl) 32(stl) 38(stl) 41(stl) 45(stl) 46(stl) 47(stl) 48(stl) 54(stl) 59(stl) 2(sfpn) 4(sfpn) 11(sfpn) 24(sfpn) 26(sfpn) 27(sfpn) 


[37;1m[38;5;164m UE complex (6) :
	Nombre de groupes : 2
	Capacite totale d'accueil: 82
	Equilibre? 20.0%: Oui
	Nombre Etudiants interesses: 76
	Nombre Etudiants effectivement inscrits : 75
	Les (1) Non-inscrits : 48(and) 
	Les Inscrits:
		Groupe 1 [45/50] : 20(res) 44(res) 46(res) 2(dac) 4(dac) 22(dac) 23(dac) 30(dac) 7(sar) 26(sar) 1(ima) 2(ima) 7(ima) 1(and) 4(and) 6(and) 10(and) 17(and) 21(and) 24(and) 30(and) 36(and) 37(and) 38(and) 45(and) 46(and) 23(stl) 46(stl) 2(sfpn) 3(sfpn) 5(sfpn) 7(sfpn) 8(sfpn) 10(sfpn) 11(sfpn) 13(sfpn) 17(sfpn) 18(sfpn) 19(sfpn) 20(sfpn) 21(sfpn) 23(sfpn) 24(sfpn) 25(sfpn) 26(sfpn) 
		Groupe 2 [30/32] : 10(ima) 1(bim) 6(bim) 8(bim) 16(bim) 25(bim) 2(and) 3(and) 7(and) 11(and) 12(and) 20(and) 25(and) 26(and) 28(and) 32(and) 35(and) 39(and) 40(and) 42(and) 43(and) 47(and) 10(stl) 6(sfpn) 9(sfpn) 12(sfpn) 14(sfpn) 15(sfpn) 16(sfpn) 22(sfpn) 


[37;1m[38;5;112m UE bima (5) :
	Nombre de groupes : 1
	Capacite totale d'accueil: 32
	Equilibre? 20.0%: Oui
	Nombre Etudiants interesses: 31
	Nombre Etudiants effectivement inscrits : 31
	Les (0) Non-inscrits : 
	Les Inscrits:
		Groupe 1 [31/32] : 35(res) 45(res) 4(dac) 9(dac) 15(dac) 26(dac) 1(ima) 2(ima) 3(ima) 5(ima) 6(ima) 7(ima) 8(ima) 11(ima) 12(ima) 2(bim) 8(bim) 12(bim) 13(bim) 19(bim) 24(bim) 25(bim) 3(and) 4(and) 15(and) 16(and) 17(and) 23(and) 24(and) 43(and) 48(and) 


[37;1m[38;5;6m UE ares (4) :
	Nombre de groupes : 3
	Capacite totale d'accueil: 96
	Equilibre? 20.0%: Oui
	Nombre Etudiants interesses: 89
	Nombre Etudiants effectivement inscrits : 89
	Les (0) Non-inscrits : 
	Les Inscrits:
		Groupe 1 [29/32] : 4(res) 7(res) 28(res) 30(res) 31(res) 33(res) 36(res) 42(res) 48(res) 50(res) 1(sar) 15(sar) 20(sar) 23(sar) 6(sfpn) 9(sfpn) 12(sfpn) 15(sfpn) 16(sfpn) 27(sfpn) 3(sesi) 25(sesi) 29(sesi) 31(sesi) 33(sesi) 36(sesi) 38(sesi) 39(sesi) 45(sesi) 
		Groupe 2 [30/32] : 1(res) 11(res) 17(res) 20(res) 23(res) 24(res) 25(res) 26(res) 43(res) 44(res) 46(res) 51(res) 2(sar) 3(sar) 11(sar) 25(sar) 26(sar) 1(sfpn) 3(sfpn) 4(sfpn) 7(sfpn) 8(sfpn) 10(sfpn) 11(sfpn) 14(sfpn) 17(sfpn) 20(sfpn) 22(sfpn) 25(sfpn) 49(sesi) 
		Groupe 3 [30/32] : 2(res) 3(res) 5(res) 6(res) 10(res) 12(res) 14(res) 15(res) 16(res) 21(res) 27(res) 34(res) 35(res) 37(res) 38(res) 39(res) 40(res) 41(res) 45(res) 47(res) 49(res) 10(sar) 13(sar) 21(sar) 22(sar) 33(sar) 50(stl) 14(sesi) 44(sesi) 47(sesi) 


[37;1m[38;5;5m UE archi1 (3) :
	Nombre de groupes : 4
	Capacite totale d'accueil: 128
	Equilibre? 20.0%: Oui
	Nombre Etudiants interesses: 76
	Nombre Etudiants effectivement inscrits : 76
	Les (0) Non-inscrits : 
	Les Inscrits:
		Groupe 1 [19/32] : 5(res) 17(res) 24(res) 26(res) 27(res) 4(sar) 22(sar) 1(sfpn) 8(sfpn) 9(sfpn) 10(sfpn) 12(sfpn) 18(sfpn) 7(sesi) 13(sesi) 22(sesi) 24(sesi) 32(sesi) 47(sesi) 
		Groupe 2 [19/32] : 33(res) 36(res) 37(res) 8(sar) 59(stl) 27(sfpn) 3(sesi) 16(sesi) 18(sesi) 20(sesi) 23(sesi) 25(sesi) 30(sesi) 31(sesi) 36(sesi) 37(sesi) 44(sesi) 49(sesi) 50(sesi) 
		Groupe 3 [19/32] : 9(sar) 10(sar) 1(sesi) 2(sesi) 4(sesi) 5(sesi) 6(sesi) 8(sesi) 9(sesi) 10(sesi) 11(sesi) 12(sesi) 19(sesi) 28(sesi) 35(sesi) 40(sesi) 41(sesi) 42(sesi) 48(sesi) 
		Groupe 4 [19/32] : 19(res) 21(res) 23(res) 41(res) 49(res) 51(res) 1(sar) 7(sar) 13(sar) 17(sesi) 21(sesi) 26(sesi) 29(sesi) 33(sesi) 34(sesi) 39(sesi) 43(sesi) 45(sesi) 46(sesi) 


[37;1m[38;5;2m UE algav (2) :
	Nombre de groupes : 3
	Capacite totale d'accueil: 96
	Equilibre? 20.0%: Oui
	Nombre Etudiants interesses: 69
	Nombre Etudiants effectivement inscrits : 69
	Les (0) Non-inscrits : 
	Les Inscrits:
		Groupe 1 [23/32] : 2(res) 15(res) 7(sar) 33(sar) 1(stl) 5(stl) 9(stl) 10(stl) 19(stl) 24(stl) 34(stl) 35(stl) 36(stl) 43(stl) 46(stl) 47(stl) 48(stl) 49(stl) 50(stl) 53(stl) 56(stl) 57(stl) 58(stl) 
		Groupe 2 [23/32] : 25(res) 32(sar) 3(ima) 2(stl) 4(stl) 6(stl) 7(stl) 12(stl) 13(stl) 14(stl) 15(stl) 17(stl) 18(stl) 20(stl) 22(stl) 26(stl) 30(stl) 38(stl) 41(stl) 45(stl) 54(stl) 55(stl) 59(stl) 
		Groupe 3 [23/32] : 8(sar) 9(sar) 15(sar) 27(sar) 31(sar) 11(ima) 17(bim) 3(stl) 8(stl) 11(stl) 16(stl) 21(stl) 25(stl) 27(stl) 32(stl) 37(stl) 39(stl) 40(stl) 42(stl) 44(stl) 51(stl) 52(stl) 46(sesi) 


[37;1m[38;5;88m UE aagb (1) :
	Nombre de groupes : 1
	Capacite totale d'accueil: 32
	Equilibre? 20.0%: Oui
	Nombre Etudiants interesses: 26
	Nombre Etudiants effectivement inscrits : 26
	Les (0) Non-inscrits : 
	Les Inscrits:
		Groupe 1 [26/32] : 6(ima) 1(bim) 2(bim) 7(bim) 8(bim) 10(bim) 12(bim) 13(bim) 14(bim) 15(bim) 16(bim) 18(bim) 19(bim) 20(bim) 21(bim) 22(bim) 23(bim) 24(bim) 25(bim) 8(and) 9(and) 12(and) 30(and) 33(and) 38(stl) 52(stl) 


[37;1m			[32;1m* * * * * * * * * * ^^ DETAIL DES AFFECTATIONS PAR UE (PLUS HAUT) ^^ * * * * * * * * * *[37;1m

[38;5;65mNombre Total d'inscriptions a satisfaire [37;1m: 1546 [38;5;65m
Nombre Maximal d'inscriptions pouvant etre satisfaites [37;1m: 1876[38;5;65m
Nombre total d'etudiants du master [37;1m: 342[38;5;65m
Charge [37;1m: 82.41 % 
[38;5;65mDesequilibre maximal autorise [37;1m: 5.0 %[38;5;65m
Caracteristiques de l'EDT [37;1m:
	[38;5;108mNombre total de contrats incompatibles (de taille 5) [37;1m: 0
	[38;5;108mPar parcours [37;1m: 

					* * * * * ^^ AUTRES INFORMATIONS ^^ * * * * *

[38;5;65mNombre d'inscriptions satisfaites [37;1m: 1527 soit 98.77%
[38;5;65mNombre d'etudiants entierement satisfaits [37;1m: 324 soit 94.74%
[38;5;65mDetail des inscriptions non satisfaites [37;1m: 
		[38;5;108mNombre de demandes non satisfaites par parcours [37;1m:
			and(5)[89.58%]  bim(0)[100.0%]  dac(3)[91.89%]  ima(0)[100.0%]  res(3)[94.12%]  sar(2)[93.94%]  sesi(0)[100.0%]  sfpn(0)[100.0%]  stl(5)[91.53%]  
		[38;5;108mNombre de demandes non satisfaites par UE (**Saturee)[37;1m:
			complex(1 / 7)  il(18 / 36)  

			[32;1m* * * * * * * * *  DONNEES RECAPITULATIVES DE L'AFFECTATION  * * * * * * * * *[37;1m


__________________________________________________________________________________________________________________________________________________________________________________