import csv
import matplotlib
import matplotlib.pyplot as plt
import os
import random
from PIL import Image
from heapq import heappop, heappush

class Analyzer:

    def __init__(self, optimizer):
        self.ListeMatchingModel = list()
        self.optimizer = optimizer
        self.dico_recapitulatif_interet_ue_conseillees_par_parcours = dict()
        self.id_subplot = 1
        self.colorNames = list(matplotlib.colors.cnames.keys())

    def add_MatchingModel(self, MM):
        self.ListeMatchingModel.append(MM)


    def etoile_si_moyenne_sup_a_capacite_un_groupe(self, ue):
        L = self.optimizer.DictUEs[ue].ListeCapacites
        if - self.dico_etat_demande_ue[ue] > min(L):
            return "*"
        return ""

    def analyze(self):
        self.dico_etat_demande_ue = {ue.intitule : 0 for ue in self.optimizer.ListeDesUEs[1:]}
        Liste_idMM = list()
        Liste_charge = list()
        Liste_satisfaction_Y = list()
        Liste_satisfaction_N = list()



        for MM in self.ListeMatchingModel:
            Liste_idMM.append(MM.get_identifiantModele())
            Liste_charge.append(MM.get_charge())
            Liste_satisfaction_N.append(MM.get_PsatisfactionN())
            Liste_satisfaction_Y.append(MM.get_PsatisfactionY())
            for ue, surbook in MM.DictUeSurdemandees.items():
                self.dico_etat_demande_ue[ue] += surbook

        #normalisation
        for ue in self.dico_etat_demande_ue:
            self.dico_etat_demande_ue[ue] /= len(self.ListeMatchingModel)

        L = list(self.dico_etat_demande_ue.keys())
        L.sort()
        plt.figure(1, figsize=(15,10))
        plt.bar(range(len(L)), [self.dico_etat_demande_ue[ue] for ue in L], width = 0.4, color=[self.colorNames[random.randint(0,len(self.colorNames)-1)] for i in range(len(L))], edgecolor = 'black')

        plt.xticks([x for x in range(len(L))], [ue+self.etoile_si_moyenne_sup_a_capacite_un_groupe(ue) for ue in L], rotation='vertical')
        plt.suptitle("Analyse UE susceptibles d'etre saturees : Mesure de la sur-demande")
        plt.title("Une * symbolise le fait que la moyenne des places encore disponibles apres affectation \ndepasse la capacite d'au moins un groupe de l'ue concernee et suggere donc une suppression.")
        plt.savefig(".ue_saturees.png")
        img = Image.open(".ue_saturees.png")
        img.show()
        plt.close()



        L_Y_copy = [val for val in Liste_satisfaction_Y]
        L_Y_copy.sort()
        L_N_copy = [val for val in Liste_satisfaction_N]
        L_N_copy.sort()
        labelY = '%Inscriptions satisfaites. Min : {}% Mediane : {}% Max : {}%'.format(L_Y_copy[0], L_Y_copy[len(L_Y_copy)//2], L_Y_copy[-1])
        labelN = '%Etudiants satisfaits. Min : {}% Mediane : {}% Max : {}%'.format(L_N_copy[0], L_N_copy[len(L_N_copy)//2], L_N_copy[-1])
        fig = plt.figure(1, figsize=(18,12))
        plt.scatter(Liste_charge, Liste_satisfaction_Y, c='y', label=labelY)

        plt.scatter(Liste_charge, Liste_satisfaction_N, c='r', label=labelN)

        for i in range(len(Liste_idMM)):
            plt.annotate(Liste_idMM[i], (Liste_charge[i], Liste_satisfaction_Y[i]))
        for i in range(len(Liste_idMM)):
            plt.annotate(Liste_idMM[i], (Liste_charge[i], Liste_satisfaction_N[i]))

        plt.legend(bbox_to_anchor=(0.5, 0), loc="lower center", bbox_transform=fig.transFigure)
        plt.subplots_adjust(bottom=0.125)

        plt.title("Mesure de la resistance d'un EDT : \nEvolution des pourcentages de satisfaction en fonction de la charge.")
        plt.grid(True)
        plt.savefig(".resistance_edt.png")
        img = Image.open(".resistance_edt.png")
        img.show()
        plt.close()

    def calculer_interet_pour_ue_conseillees_par_parcours(self, dossierVoeux=''):
        if dossierVoeux == '':
            print "analyse des donnees courantes"
        else:
            for fichierVoeux in os.listdir(dossierVoeux):
                try: #POUR EVITER LES ERREURS DE SPLIT SUR LE DOSSIER DE VOEUX PAR PARCOURS
                    parcours = fichierVoeux.split('.')[1]
                    path = dossierVoeux+"/"+fichierVoeux
                    f_voeux = open(path)
                    data = csv.DictReader(f_voeux)
                    D = dict()
                    for ligneEtu in data:
                        ListeUEsConseilleesDeEtudiant = [ligneEtu["cons"+str(id)] for id in range(1, self.optimizer.Parameters.nbMaxUEConseillees+1) if ligneEtu["cons"+str(id)] != ""]
                        for ue in ListeUEsConseilleesDeEtudiant:
                            if ue not in D:
                                D[ue] = 1
                            else:
                                D[ue] += 1
                    self.dico_recapitulatif_interet_ue_conseillees_par_parcours[parcours] = D
                except:
                    print "Erreur lors de la lecture du dossier : {}".format(dossierVoeux)
                    pass
            for parcours, D in self.dico_recapitulatif_interet_ue_conseillees_par_parcours.items():
                self.generer_histogramme_des_interest(parcours, D)
            self.id_subplot = 1  # evite les crash de reouverture
            plt.suptitle("Mesure du choix des UE conseillees en fonction du parcours")
            plt.gcf().subplots_adjust(hspace=0.5)
            plt.savefig(".interets_ue_conseilles.png")
            # plt.show()
            img = Image.open(".interets_ue_conseilles.png")
            img.show()
            # print("Close")
            plt.close()

    def generer_histogramme_des_interest(self, parcours, D):
        Liste_ues = list()
        Liste_effectif = list()

        color = random.randint(0,len(self.colorNames)-1)
        plt.figure(1, figsize=(15,10))
        for ue, effectif in D.items():
            Liste_ues.append(ue)
            Liste_effectif.append(effectif)
        plt.subplot(3,3,self.id_subplot)
        self.id_subplot += 1

        plt.bar(range(len(Liste_ues)), Liste_effectif, width=0.4, color=self.colorNames[color], edgecolor='black',label=parcours)  # color=[colorNames[random.randint(0,len(colorNames)-1)] for i in range(len(Liste_effectif))])
        plt.xticks([x for x in range(len(Liste_effectif))],[str(Liste_ues[i]) + "(" + str(Liste_effectif[i]) + ")" for i in range(len(Liste_ues))],rotation='vertical')
        plt.legend(loc=0)
        #
        # plt.title("Mesure du choix des UE conseillees en fonction du parcours : {}".format(parcours))
        # plt.show()

    def reset(self):
        self.ListeMatchingModel = list()
        self.dico_recapitulatif_interet_ue_conseillees_par_parcours = dict()
        self.id_subplot = 1
        self.dico_etat_demande_ue = dict()


    def pie_chart_effectif_parcours(self):
        self.dico_effectif = dict()
        self.dico_taille_voeux = dict()
        if self.optimizer.voeux_charges:
            for ParcoursO in self.optimizer.ListeDesParcours:
                self.dico_effectif[ParcoursO.nom] = 0
                self.dico_taille_voeux[ParcoursO.nom] = 0

            for Etu in self.optimizer.ListeDesEtudiants:
                self.dico_effectif[Etu.Parcours.nom] += 1
                self.dico_taille_voeux[Etu.Parcours.nom] += Etu.nombreDeVoeux
            labels = list()
            L_effectif = list()
            colors = list()
            for lab, eff in self.dico_effectif.items():
                labels.append(lab)
                L_effectif.append(eff)
                color = random.randint(0,len(self.colorNames)-1)
                colors.append(self.colorNames[color])
            plt.subplot(1,2,1)
            plt.pie(L_effectif, labels=labels, colors=colors,autopct=lambda x: str(round(x, 2)) + '%',pctdistance=0.7,labeldistance=1.2)
            plt.legend()
            plt.title("Effectifs des differents parcours du Master Informatique")
            labels = list()
            L_taille = list()
            for lab, demandes in self.dico_taille_voeux.items():
                labels.append(lab)
                L_taille.append(demandes)
            plt.subplot(1,2,2)
            plt.pie(L_taille, labels=labels, colors=colors,autopct=lambda x: str(round(x, 2)) + '%',pctdistance=0.7,labeldistance=1.2)
            plt.legend()
            plt.title("Taille de la demande globale d'inscriptions aux UE des differents parcours du Master Informatique")
            # plt.savefig(".EfffectifMaster_tailleDemandeGlobaleParParcours.jpeg")
            plt.show()
            # img = Image.open(".EfffectifMaster_tailleDemandeGlobaleParParcours.jpeg")
            # img.show()
            plt.close()

    def generer_distribution_taille_voeux_parcours(self):
        id_subpot = 0
        self.dico_distribution_taille_voeux = dict()
        if self.optimizer.voeux_charges:
            for ParcoursO in self.optimizer.ListeDesParcours:
                self.dico_distribution_taille_voeux[ParcoursO.nom] = [0 for i in range(self.optimizer.Parameters.TailleMaxContrat+1)]

            for Etu in self.optimizer.ListeDesEtudiants:
                self.dico_distribution_taille_voeux[Etu.Parcours.nom][Etu.nombreDeVoeux] += 1

            for parcours, L in self.dico_distribution_taille_voeux.items():
                id_subpot += 1
                plt.subplot(3, 3, id_subpot)
                color = random.randint(0,len(self.colorNames)-1)
                moyenne = 1.0*sum([L[i]*i for i in range(len(L))])/sum(L)
                # print parcours, L ,
                plt.bar(range(len(L)), L, width=0.4, color=self.colorNames[color], edgecolor='black',label=parcours+" ({})[{}%]".format(round(moyenne,2), round(100.0*sum(L[:5])/sum(L), 1)))
                plt.legend(loc=0)
                plt.suptitle("Distribution de la taille des voeux par parcours\n(moyenne du nombre d'UE/Etudiant)\n[Pourcentage de redoublants]")
            plt.show()

    def caracteristiques_UE(self):
        T = list()
        if self.optimizer.edt_charge and self.optimizer.parcours_charge:
            for Ue in self.optimizer.ListeDesUEs[1:]:
                capaciteTotale = Ue.capaciteTotale
                nb_groupes = Ue.nb_groupes
                etendue = Ue.etendue
                obligatoires_en = list()
                conseillees_en = list()
                for Parc in self.optimizer.ListeDesParcours:
                    for ue_str in Parc.ListeUEObligatoires:
                        if Ue.intitule == ue_str:
                            obligatoires_en.append(Parc.nom)
                    for ue_str in Parc.ListeUEConseilles:
                        if Ue.intitule == ue_str:
                            conseillees_en.append(Parc.nom)
                tuple_ = capaciteTotale, nb_groupes, etendue, obligatoires_en, conseillees_en, Ue.intitule
                heappush(T, tuple_)
            # T.reverse()
            # for i in range(1, len(T)+1):
            #     print T[-i]
            new_T = list()
            while len(T) != 0:
                new_T.append(heappop(T))

            f = open("tablo.csv", "w")
            fieldnames = ["UE", "Cap. totale", "Nb. grp.", "Etendue", "Oblig. en", "Cons. en"]
            writer = csv.DictWriter(f, fieldnames=fieldnames)
            writer.writeheader()
            for i in range(1, len(new_T)+1):
                csvLine = dict()
                capaciteTotale, nb_groupes, etendue, obligatoires_en, conseillees_en, intitule = new_T[-i]
                csvLine["UE"] = intitule.upper()
                csvLine["Cap. totale"] = capaciteTotale
                csvLine["Nb. grp."] = nb_groupes
                csvLine["Etendue"] = etendue
                chaine_obligato = ""
                if len(obligatoires_en) == 0:
                    chaine_obligato = "-"
                else:
                    for p in obligatoires_en[1:]:
                        chaine_obligato += "-" + p.upper()
                    chaine_obligato = obligatoires_en[0].upper() + chaine_obligato
                csvLine["Oblig. en"] = chaine_obligato
                chaine_cons = ""
                if len(conseillees_en) == 0:
                    chaine_cons = "-"
                else:
                    for p in conseillees_en[1:]:
                        chaine_cons += "-" + p.upper()
                    chaine_cons = conseillees_en[0].upper() + chaine_cons
                csvLine["Cons. en"] = chaine_cons
                writer.writerow(csvLine)
            f.close()

    def RL_analyse(self, L_Y, L_N):
        nb_total_inscriptions_a_satisfaire = 0
        for Etu in self.optimizer.ListeDesEtudiants:
            nb_total_inscriptions_a_satisfaire += Etu.nombreDeVoeux
        plt.subplot(2, 1, 1)
        plt.plot(range(len(L_Y)), L_Y, color='y', label="Nb. d' insc. satisf.")
        plt.plot(range(len(L_Y)), [nb_total_inscriptions_a_satisfaire for i in range(len(L_Y))], color='b', label="Nb. tot. d'insc. a satisf.")
        plt.legend()
        plt.title("Evolution du nombre de demandes d'inscription satisfaites (Recherche Locale avec indifference en {} iterations)".format(len(L_Y)))
        # plt.savefig(".RL_Y.jpeg")
        plt.subplot(2, 1, 2)
        plt.plot(range(len(L_N)), L_N, color='r', label="Nb. d'etu. entierem. satisf.")
        plt.plot(range(len(L_N)), [len(self.optimizer.ListeDesEtudiants) for i in range(len(L_N))], color='b', label="Nb. tot. d'etu. du Master")
        plt.legend()
        plt.title("Evolution du nombre nombre d'etudiants entierement satisfaits (Recherche Locale avec indifference en {} iterations)".format(len(L_N)))
        plt.savefig(".RL.jpeg")
        # img = Image.open(".RL.jpeg")
        # img.show()
        plt.show()
